﻿/*
 * Author:Pol Egea
 * Date: 17-1-2022
 * Description: Test dels exercicis de parametrització
 */
using NUnit.Framework;
using ExercicisOptimitzacio;

namespace TestExercicis
{
    [TestFixture]
    public class IsLeapYear
    {
        [Test]
        public void AnyTraspas()
        {
            Assert.AreEqual(true,ExercicisTests.Multiples(2024));
        }
        [Test]
        public void NoAnyTraspas()
        {
            Assert.AreEqual(false,ExercicisTests.Multiples(2022));
        }
    }
    [TestFixture]
    public class MinOf10Values
    {
        [Test]
        public void Normal()
        {
            int[] valors = new int[] {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
            Assert.AreEqual(0,ExercicisTests.Minim(valors));
        }
        [Test]
        public void ValorsNegatius()
        {
            int[] valors = new int[] {9, 8, 7, 6, -20, 4, -9, 2, 1, 0};
            Assert.AreEqual(-20,ExercicisTests.Minim(valors));
        }
    }
    [TestFixture]
    public class Dni
    {
        [Test]
        public void Normal()
        {
            Assert.AreEqual("X",ExercicisTests.TractamentLletra("47242700"));
        }
    }
}