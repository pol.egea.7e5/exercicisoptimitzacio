﻿/*
 * Author:Pol Egea
 * Date: 17-1-2022
 * Description: Varietat d'exercicis realitzats amb parametrització: IsLeapYear mostra si l'any és de traspàs
 *              MinOf10Values mostra el número mínim de 10 valors
 *              HowManyDays mostra el numero de dies en funció del mes
 *              DNI fa el calcul per a coneixer l'ultim digit del DNI
 */

using System;
using System.Linq;

namespace ExercicisOptimitzacio
{
    public class ExercicisTests
    {
        private static void Main()
        {
            var programa = new ExercicisTests();
            programa.Inici();
        }

        private void Inici()
        {
            var fi = false;
            do
            {
                MostraMenu();
                TractamentMenu(ref fi);
            } while (!fi);
        }

        public void MostraMenu()
        {
            Console.WriteLine("Benvinguts a Exercicis Optimització");
            Console.WriteLine("/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\-/-\\");
            Console.WriteLine("IL- IsLeapYear");
            Console.WriteLine("MO- MinOf10Values");
            Console.WriteLine("HM- HowManyDays");
            Console.WriteLine("DNI- DNI");
            Console.WriteLine("FI- Finalitza programa");
            Console.WriteLine();
            Console.WriteLine("Introdueixi el nom del programa");
        }

        public void TractamentMenu(ref bool fi)
        {
            var nomProg = Console.ReadLine();
            switch (nomProg)
            {
                case "IL":
                    IsLeapYear();
                    break;
                case "MO":
                    MinOf10Values();
                    break;
                case "HM":
                    HowManyDays();
                    break;
                case "DNI":
                    Dni();
                    break;
                case "FI":
                    fi = true;
                    break;
                default:
                    Console.WriteLine("Error: Dada mal introduida");
                    break;
            }
        }

        public void Dni()
        {
            string valorsDni= IntroduirDni();
            TractamentDni(ref valorsDni);
        }

        string IntroduirDni()
        {
            string valorsDni;
            do
            {
                Console.WriteLine("Introdueixi 8 digits per al DNI");
                valorsDni = Console.ReadLine();
            } while (!RevisaDades(valorsDni));

            return valorsDni;
        }

        public static bool RevisaDades(string valorsDni)
        {
            var valorCorrecte = true;
            if (valorsDni.Length == 8)
                for (var i = 0; i < valorsDni.Length; i++)
                    switch (valorsDni[i])
                    {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            break;
                        default:
                            valorCorrecte = false;
                            break;
                    }
            else valorCorrecte = false;

            return valorCorrecte;
        }

        public void TractamentDni(ref string valorsDni)
        {
            var lletra = TractamentLletra(valorsDni);
            Console.WriteLine("{0} + {1}", valorsDni, lletra);
        }

        public static string TractamentLletra(string valorsDni)
        {
            var numDni = Convert.ToInt32(valorsDni) % 23;
            var lletra = "";
            switch (numDni)
            {
                case 0:
                    lletra = "T";
                    break;
                case 1:
                    lletra = "R";
                    break;
                case 2:
                    lletra = "W";
                    break;
                case 3:
                    lletra = "A";
                    break;
                case 4:
                    lletra = "G";
                    break;
                case 5:
                    lletra = "M";
                    break;
                case 6:
                    lletra = "Y";
                    break;
                case 7:
                    lletra = "F";
                    break;
                case 8:
                    lletra = "P";
                    break;
                case 9:
                    lletra = "D";
                    break;
                case 10:
                    lletra = "X";
                    break;
                case 11:
                    lletra = "B";
                    break;
                case 12:
                    lletra = "N";
                    break;
                case 13:
                    lletra = "J";
                    break;
                case 14:
                    lletra = "Z";
                    break;
                case 15:
                    lletra = "S";
                    break;
                case 16:
                    lletra = "Q";
                    break;
                case 17:
                    lletra = "V";
                    break;
                case 18:
                    lletra = "H";
                    break;
                case 19:
                    lletra = "L";
                    break;
                case 20:
                    lletra = "C";
                    break;
                case 21:
                    lletra = "K";
                    break;
                case 22:
                    lletra = "E";
                    break;
            }

            return lletra;
        }

        public void IsLeapYear()
        {
            var any = IntrodueixDades();
            TractamentDades(ref any);
        }

        public void MinOf10Values()
        {
            var valors = new int[10];
            Introdueix10Valors(valors);
            TrobaMinim(valors);
        }

        public void HowManyDays()
        {
            var mes = RevisaMes();
            var any = RevisaAny();
            OperaMesAny(ref mes, ref any);
        }

        public static int RevisaMes()
        {
            int mes;
            do
            {
                Console.WriteLine("Introdueixi un mes entre 1 i 12");
                mes = Convert.ToInt32(Console.ReadLine());
            } while (mes>13||mes<1);

            return mes;
        }

        public static int RevisaAny()
        {
            int any;
            do
            {
                Console.WriteLine("Introdueixi un any després de Crist");
                any = Convert.ToInt32(Console.ReadLine());
            } while (any <= -1);

            return any;
        }

        public void OperaMesAny(ref int mes, ref int any)
        {
            switch (mes)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    Console.WriteLine("31 dies");
                    break;
                case 2:
                    if (Multiples(any)) Console.WriteLine("El més té 29 dies");
                    else Console.WriteLine("El més té 28 dies");
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    Console.WriteLine("El més te 30 dies");
                    break;
            }
        }

        public void Introdueix10Valors(int[] valors)
        {
            Console.WriteLine("Introdueixi 10 valors");
            for (var j = 0; j < valors.Length; j++)
            {
                Console.WriteLine("Introdueixi valor número {0}", j + 1);
                valors[j] = Convert.ToInt32(Console.ReadLine());
            }
        }

        public void TrobaMinim(int[] valors)
        {
            Console.WriteLine("El valor mínim és {0}", Minim(valors));
        }

        public static int Minim(int[] valors)
        {
            return valors.Min();
        }

        public static int IntrodueixDades()
        {
            Console.WriteLine("Introdueixi any");
            int any = Convert.ToInt32(Console.ReadLine());
            return any;
        }

        public void TractamentDades(ref int any)
        {
            if (NumCorrecte(any))
            {
                if (Multiples(any))
                    Console.WriteLine("{0},és de traspàs", any);
                else
                    Console.WriteLine("{0} no és de traspàs", any);
            }
            else
            {
                Console.WriteLine("Error,Dada mal introduida");
            }
        }

        public static bool NumCorrecte(int any)
        {
            bool comprova;
            if (any > -1)
                comprova = true;
            else
                comprova = false;

            return comprova;
        }

        public static bool Multiples(int any)
        {
            if (any % 4 == 0 && any % 100 != 0)
                return true;
            if (any % 100 == 0 && any % 400 == 0)
                return true;
            return false;
        }
    }
}